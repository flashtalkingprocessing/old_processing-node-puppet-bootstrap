Processing Cluster Bootstrap
==================
Unified Installer
-----------------------------

To install run:

goo.gl short URL `sudo bash -c "$(curl -fsSL http://goo.gl/L6cW7V)"`

Or the full URL

`sudo bash -c "$(curl -fsSL https://bitbucket.org/flashtalkingprocessing/processing-node-puppet-bootstrap/raw/master/bootstrap.sh)"`

This is a new release so please report any issues to Adam Williams <adam.williams@flashtalking.com>.