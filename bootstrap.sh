#!/bin/bash

read -e -p "Enter your BitBucket username: " username
read -e -p "Enter your BitBucket password: " password


if [ -z "${username}" ]; then
    echo "Error: No BitBucket credentials specified."
    exit
fi

ROLES[1]=management_web
ROLES[2]=management_mysql
ROLES[3]=managament_redis
ROLES[4]=processing
ROLES[5]=monitoring

echo "
"

echo "Deployment Roles:\n"


for i in "${!ROLES[@]}"
do
  echo " * $i ) ${ROLES[$i]}"
done
read -e -p "Enter the role number you wish to deploy: " ROLE


LOGFILE="/tmp/bootstrap.log"

cd /tmp
echo "" > $LOGFILE

if [ -z "${ROLES[$ROLE]}" ]; then
   echo "Error - No valid role specified. Please re-run choosing a valid role."
   exit 1
fi

ROLE=${ROLES[$ROLE]}

read -r -p "Installing configs for role: $ROLE. Continue? [y/N] " response

case $response in
    [yY][eE][sS]|[yY])
        echo "Okay, if you're sure..."
        ;;
    *)
        echo "Exiting..."
        exit
        ;;
esac

ROLE_GIT=`/bin/echo "$ROLE" | /bin/sed 's/_/-/g'`
ROLE_NICE=`/bin/echo "$ROLE" | /bin/sed 's/_/ /g'`
ROLE_NICE=`read -ra words <<< "$ROLE_NICE" && /bin/echo "${words[@]^}"`

echo -ne "\n\n$ROLE_NICE Node Setup Utility\n"
echo "---------------------------------------"
echo -ne "\n\n"

# Get Node Identifier
NODE_IP=''
NODE_UID=''

if `ifconfig eth0 &> /dev/null`; then
    NODE_IP=`ifconfig eth0 | grep 'inet addr:' | awk -F ' ' '{print  $2}' | awk -F ':' '{print $2}'`
    NODE_UID="node_`echo $NODE_IP | sed 's/[\._-]//g'`"
    
    echo -ne "Your Node ID is: $NODE_UID\n"
elif `ifconfig em1 &> /dev/null`; then
    NODE_IP=`ifconfig em1 | grep 'inet addr:' | awk -F ' ' '{print  $2}' | awk -F ':' '{print $2}'`
    NODE_UID="node_`echo $NODE_IP | sed 's/[\._-]//g'`"

    echo -ne "Your Node ID is: $NODE_UID\n"
else
    echo "Error: Unable to create node identifier. Exiting."
    exit 1
fi

echo -ne "Detecting OS... "
if [ ! -f "/usr/bin/yum" ]; then 
    echo -ne "Unsupported OS. Exiting.\n"
    exit 1
fi
echo -ne "Red Hat based\n"

echo -ne "Detecting Puppet... "

if [ ! -f "/usr/bin/puppet" ]; then
    /usr/bin/yum install -y https://yum.puppetlabs.com/el/6/products/x86_64/puppetlabs-release-6-7.noarch.rpm &> $LOGFILE
    /usr/bin/yum install puppet -y &> $LOGFILE
fi

if [ ! -f "/usr/bin/puppet" ]; then
    echo -ne "Failed. Are you root?\n"
    exit 1
else
    echo -ne "Installed\n"
fi

echo -ne "Detecting Git... "
if [ ! -f "/usr/bin/git" ]; then
    /usr/bin/yum install git -y &> $LOGFILE
fi

if [ ! -f "/usr/bin/git" ]; then
    echo -ne "Failed. See $LOFILE for details.\n"
    exit 1
else
    echo -ne "Installed\n"
fi

echo -ne "Cloning Puppet configs... "
/bin/rm -rf /etc/puppet
if /usr/bin/git clone https://${username}:${password}@bitbucket.org/flashtalkingprocessing/processing-node-puppet.git /etc/puppet &> $LOGFILE; then
    echo -ne "Done\n"
else
    echo -ne "Failed. Exiting. See $LOGFILE for details.\n"
    exit 2
fi

echo -ne "Applying Puppet configs... "

# Write node.pp file
echo "class { 'base_packages': } ->
class { 'role_${ROLE}_node': }" > /etc/puppet/manifests/node.pp

if [ -f "/etc/puppet/manifests/node.pp" ]; then
    if /usr/bin/puppet apply /etc/puppet/manifests/node.pp &> $LOGFILE; then
        echo -ne "Done\n"
    else
	echo -ne "Failed. Exiting. See $LOGFILE for details.\n"
    	exit 3
    fi
else
    echo -ne "Failed. Could not clone Git repo. Exiting. See $LOGFILE for details.\n"
    exit 4
fi

echo -ne "Checking for node specific configs... "
if [ -z $NODE_UID ]; then
    echo -ne "Failed. Unable to disover Node ID. See $LOGFILE for details.\n"
    exit 5
else
    if [ -f "/etc/puppet/manifests/$NODE_UID.pp" ]; then
	if /usr/bin/puppet apply /etc/puppet/manifests/$NODE_UID.pp &> $LOGFILE; then
	    echo -ne "Done\n"
	else
	    echo -ne "Failed. See $LOGFILE for details.\n"
	fi
    else
	echo -ne "No node specific configs found.\n"
    fi	
fi

echo -ne "\n\nBootstrap complete.\n\n"

echo -ne "Log:\n\n"
cat /tmp/bootstrap.log
